from django.apps import AppConfig


class ActionAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'action_app'
