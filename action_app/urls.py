from django.urls import path
from action_app import views

app_name = 'action_app'
urlpatterns = [
    path('make_action/', views.make_action, name='make_action'),
]