from django.shortcuts import render

def make_action(request):
    return render(
        request,
        'action_app/make_action.html'
    )
# Create your views here.
