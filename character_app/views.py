from django.urls import reverse_lazy
from django.shortcuts import render
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import DeleteView
from character_app.models import Character
from character_app.forms import ActionForm

# def create(request):
#                             # if request.method == "POST":
#                             #     new_value = request.POST.get('value')
#     return render(
#         request,
#         'character_app/create.html'
#     )
# Create your views here.

class CharacterListView(ListView):
    model = Character


class CharacterDetailView(DetailView):
    model = Character


class CharacterCreateView(CreateView):
    model = Character
    fields = [
        'name',
        'type',
        'attack',
        'defence',
        'health_points',
    ]

class CharacterUpdateView(UpdateView):
    model = Character
    fields = [
        'type',
        'attack',
        'defence',
        'health_points',
    ]
    template_name_suffix = "_update"

class CharacterDeleteView(DeleteView):
    model = Character
    success_url = reverse_lazy('character_app:character-list')


def fight(request):
    form = ActionForm()
    return render(
        request,
        'character_app/fight.html',
        context={
            'form': form,
        }
    )

def result(request):
    if request.method == 'POST':
        character_1_id = request.POST.get('character_1')
        character_2_id = request.POST.get('character_2')

        character_1 = Character.objects.get(id=character_1_id)
        character_2 = Character.objects.get(id=character_2_id)



        try:
            loss = character_1.attack / character_2.defence

        except ZeroDivisionError:
            loss = 0 + character_1.attack

        character_2.health_points = character_2.health_points - loss
        character_2.save()


        if character_2.health_points > 0:
            message = 'Postać ' + character_2.name + ' straciła ' + str(loss) + ' punktów życia. Posiada teraz ' + str(character_2.health_points)
        else:
            message = 'Postać ' + character_2.name + ' nie żyje :((('


        return render(
            request,
            'character_app/result.html',
            context={
                'message': message

            }
        )
