from django.contrib import admin

from character_app.models import Character
from character_app.models import Action
# Register your models here.

admin.site.register(Character)
admin.site.register(Action)
