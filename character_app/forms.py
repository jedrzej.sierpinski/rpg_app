from django import forms
from character_app.models import Action

class ActionForm(forms.ModelForm):
    class Meta:
        model = Action
        fields = [
            "character_1",
            "character_2",

        ]