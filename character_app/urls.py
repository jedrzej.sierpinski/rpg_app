from django.urls import path
from character_app import views

app_name = 'character_app'
urlpatterns = [
    path('create/', views.CharacterCreateView.as_view(), name='character-create'),
    path('list/', views.CharacterListView.as_view(), name='character-list'),
    path('detail/<int:pk>', views.CharacterDetailView.as_view(), name='character-detail'),
    path('update/<int:pk>', views.CharacterUpdateView.as_view(), name='character-update'),
    path('delete/<int:pk>', views.CharacterDeleteView.as_view(), name='character-delete'),

    path('fight/', views.fight, name='fight'),
    path('result/', views.result, name='result'),

]
