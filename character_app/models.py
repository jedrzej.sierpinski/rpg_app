from django.db import models
from django.shortcuts import reverse

CHARACTER_TYPE = [
    (1, "Przyjaciel"),
    (2, "Wróg"),
]

class Character(models.Model):
    name = models.CharField(max_length=30)
    type = models.IntegerField(choices=CHARACTER_TYPE)
    attack = models.IntegerField()
    defence = models.IntegerField()
    health_points = models.IntegerField()

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('character_app:character-list')


class Action(models.Model):
    character_1 = models.ForeignKey(Character, related_name="characters_1", on_delete=models.CASCADE)
    character_2 = models.ForeignKey(Character, related_name="characters_2", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.character_1} atakuje {self.character_2}"


